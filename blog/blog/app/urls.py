from django.conf.urls.defaults import *
from blog.app.api import EntryResource, UserResource
from tastypie.api import Api

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(EntryResource())

urlpatterns = patterns('',
    # The normal jazz here...
    (r'^api/', include(v1_api.urls)),
)
