# Create your views here.
from django.contrib.auth.forms import AuthenticationForm

def person_login(request):
    if request.method == 'POST':
        login_form = AuthenticationForm(request.POST) # Not shown in this example

        if login_form.is_valid():
            username = login_form.cleaned_data['email']
            password = login_form.cleaned_data['password']
            authenticated_user = authenticate(username=username, password=password)

            if authenticated_user:
                login(request, authenticated_user)

                return HttpResponseRedirect('/')
            else:
                # Their password / email combination must have been incorrect
                pass
        else:
            # They didn't fill out the form properly
            pass
    else:
        login_form = AuthenticationForm()

    return render(request, 'people/person_login_form.html', { 'login_form' : login_form })