from accounts.models import Person

class PersonAuthenticationBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            person = Person.objects.get(username=username)
            if person.check_password(password):
                return person
        except Person.DoesNotExist:
            pass

        return None

    def get_user(self, user_id):
        try:
            return Person.objects.get(pk=user_id)
        except Person.DoesNotExist:
            return None